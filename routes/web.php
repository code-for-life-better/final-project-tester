<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');
//Route::get('/adminlte', 'AdminlteController@adminlte');

/*=============================ROUTE MIDDLEWARE===========================================*/
Route::group(['middleware' => ['auth']], function () {

    Route::get('/biodata', 'BiodataController@index');
    //route CRUD Biodata
    //create
    Route::get('/biodata/create', 'BiodataController@create');

    Route::post('/biodata', 'BiodataController@store');

    Route::get('/biodata/{biodata_id}', 'BiodataController@show');
    Route::get('/biodata/{biodata_id}/edit', 'BiodataController@edit');
    Route::put('/biodata/{biodata_id}', 'BiodataController@update');
    Route::delete('/biodata/{biodata_id}', 'BiodataController@destroy');



    /*=============================ROUTE MATAPELAJARAN===========================================*/
    // Create
    Route::get('/mapel/create', 'MapelController@create');
    Route::post('/mapel', 'MapelController@store');

    // Read
    Route::get('/mapel', 'MapelController@index');
    Route::get('/mapel/{mapel_id}', 'MapelController@show');

    // Update
    Route::get('/mapel/{mapel_id}/edit', 'MapelController@edit');
    Route::put('/mapel/{mapel_id}/', 'MapelController@update');

    // delete
    Route::delete('/mapel/{mapel_id}/', 'MapelController@destroy');



    // Route::get('/home', 'HomeController@index')->name('home');


    /*=============================ROUTE GURU===========================================*/

    //route CRUD Guru
    Route::get('/guru', 'GuruController@index');
    //create
    Route::get('/guru/create', 'GuruController@create');

    Route::post('/guru', 'GuruController@store');

    Route::get('/guru/{guru_id}', 'GuruController@show');
    Route::get('/guru/{guru_id}/edit', 'GuruController@edit');
    Route::put('/guru/{guru_id}', 'GuruController@update');
    Route::delete('/guru/{guru_id}', 'GuruController@destroy');


    /*=============================ROUTE INPUT NILAI===========================================*/
    Route::get('/nilai_daftar_siswa', 'NilaiController@NilaiDaftarSiswa');
    Route::get('/nilai/{siswa_id}/daftar_nilai_by_siswa', 'NilaiController@DaftarNilaiBySiswa');
    Route::get('/nilai/{siswa_id}/nilai_input_nilai', 'NilaiController@InputNilai');
    Route::put('/nilai/add/{siswa_id}/', 'NilaiController@store');

    Route::delete('/nilai/{nilai_id}/{siswa_id}', 'NilaiController@destroy');
    Route::get('/nilai_edit/{nilai_id}/{siswa_id}', 'NilaiController@edit');
    Route::put('/nilai_update/{nilai_id}', 'NilaiController@update');

    /*=============================ROUTE TRANSKRIP===========================================*/
    Route::get('/transkrip', 'TranskripController@NilaiDaftarSiswa');
    Route::get('/transkrip/{siswa_id}/daftar_nilai_by_siswa', 'TranskripController@DaftarNilaiBySiswa');
    Route::put('/transkrip/{transkrip_id}/{siswa_id}', 'TranskripController@update');
    Route::get('/cetak_transkrip/{siswa_id}', 'TranskripController@CetakTranskrip');


});

Auth::routes();
