@extends('layout.layout_adminlte')

@section('judul')
	Daftar Mata Pelajaran
@endsection
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>    
@endpush

@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush

@section('content')
<a class="btn btn-success mb-3" href="/mapel/create">Tambah Mata Pelajaran</a>
<table id="example1" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Kode</th>
        <th scope="col">Nama</th>
        <th scope="col">Aksi </th>
      </tr>
    </thead>
    <tbody>
        @forelse ($mapel as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->kode_mapel}}</td>
            <td>{{$item->nama_mapel}}</td>
            <td>
                <form action="/mapel/{{$item->id}}" method="POST">
                    <a href="/mapel/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit </a>
                    @csrf
                    @method('delete')
                    <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
            <h2>Data tidak ada</h2>
        @endforelse
    </tbody>
  </table>
@endsection