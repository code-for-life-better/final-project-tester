@extends('layout.layout_adminlte')

@section('judul')
Detail Siswa {{$biodata->nama}}    {{--INI BUAT JUDUL --}}
@endsection

@section('content')

<div class="form-group">
    <label>Nama : </label>
    <input disabled type="text" value="{{$biodata->nama}}" class="form-control" name="nama">
</div>
    
<div class="form-group">
    <label for="dit_kelas">Kelas : </label>
    <input value="{{$biodata->dit_kelas}}" disabled type="text" class="form-control" name="dit_kelas">
    
</div>
 

<div class="form-group">
    <label>Tanggal Diterima di SMA S AL KAUTSAR : </label>
    <input value="{{$biodata->pada_tgl}}" disabled type="date" class="form-control" name="pada_tgl">
</div>
  
  
<div class="form-group">
    <label>Semester : </label>
    <input value="{{$biodata->semester}}" disabled placeholder="Contoh : 1 (Satu)" type="text" class="form-control" name="semester">
</div>
   

<div class="form-group">
    <label for="jn_kelamin">Jenis Kelamin : </label>
    <input value="{{$biodata->jn_kelamin}}" disabled type="date" class="form-control" name="jn_kelamin">
</div>
  
    
<div class="form-group">
    <label>NIK : </label>
    <input disabled value="{{$biodata->nik}}" required placeholder="16 digit" type="number" class="form-control" name="nik">
</div>
   

<div class="form-group">
    <label>No Kartu Keluarga : </label>
    <input disabled value="{{$biodata->no_kk}}" placeholder="16 digit"  type="number" class="form-control" name="no_kk">
</div>
    

<div class="form-group">
    <label>Tempat Lahir : </label>
    <input disabled value="{{$biodata->tm_lahir}}" type="text" class="form-control" name="tm_lahir">
</div>
 

<div class="form-group">
    <label>Tanggal Lahir : </label>
    <input disabled value="{{$biodata->tgl_lahir}}"  type="date" class="form-control" name="tgl_lahir">
</div>
    

<div class="form-group">
    <label>NISN : </label>
    <input disabled value="{{$biodata->nisn}}" required  type="number" class="form-control" name="nisn">
</div>
   
<div class="form-group">
    <label>NIS : </label>
    <input disabled value="{{$biodata->nis}}" placeholder="4 Digit Angka" type="number" class="form-control" name="nis">
</div>
   

<div class="form-group">
    <label>Nomor Ijazah SMP : </label>
    <input disabled value="{{$biodata->no_ijazah}}" type="text" class="form-control" name="no_ijazah">
</div>
  

<div class="form-group">
    <label>Tahun Ijazah SMP : </label>
    <input disabled value="{{$biodata->th_ijazah}}" type="number" class="form-control" name="th_ijazah">
</div>
    

<div class="form-group">
    <label>No Seri SKHUN / SKL SMP : </label>
    <input disabled value="{{$biodata->no_skhun}}" type="text" class="form-control" name="no_skhun">
</div>
   

<div class="form-group">
    <label>Tahun SKHUN / SKL SMP : </label>
    <input disabled value="{{$biodata->th_skhun}}" type="number" class="form-control" name="th_skhun">
</div>
    
<div class="form-group">
    <label>Gol Darah : </label>
    <input disabled value="{{$biodata->gol_darah}}" type="text" class="form-control" name="gol_darah">
</div>
  

<div class="form-group">
    <label>Agama : </label>
    <input disabled value="{{$biodata->agama}}"  type="text" class="form-control" name="agama">
</div>
   

<div class="form-group">
    <label>Kewarganegaraan : </label>
    <input disabled value="{{$biodata->kewargaan}}"  type="text" class="form-control" name="kewargaan">
</div>
  

<div class="form-group">
    <label>Alamat Rumah : </label>
    <textarea disabled value="" class="form-control" name="alamat_s">{{$biodata->alamat_s}}</textarea>
</div>
    

<div class="form-group">
    <label>Status Dalam Keluarga : </label>
    <input disabled value="{{$biodata->st_dl_kel}}" Placeholder="Anak Kandung / Anak Sambung" type="text" class="form-control" name="st_dl_kel">
</div>
   
<div class="form-group">
    <label>No Hp : </label>
    <input disabled value="{{$biodata->no_hp}}" type="number" class="form-control" name="no_hp">
</div>
 

<div class="form-group">
    <label>Asal SMP / MTS : </label>
    <input disabled value="{{$biodata->asal_smp}}" type="text" class="form-control" name="asal_smp">
</div>
 

<div class="form-group">
    <label>Asal Sekolah Pindahan : </label>
    <input disabled value="{{$biodata->asal_pindahan}}" type="text" class="form-control" name="asal_pindahan">
</div>
   

<div class="form-group">
    <label>Jarak Rumah Kesekolah : </label>
    <input disabled value="{{$biodata->jrk_tinggal}}" placeholder="Ketik dalam Km" type="text" class="form-control" name="jrk_tinggal">
</div>


<div class="form-group">
    <label>Tinggi Badan : </label>
    <input disabled value="{{$biodata->t_bdn}}" required placeholder="Contoh 172 cm" type="text" class="form-control" name="t_bdn">
</div>


<div class="form-group">
    <label>Berat Badan : </label>
    <input disabled value="{{$biodata->b_bdn}}" required placeholder="Contoh 56 Kg" type="text" class="form-control" name="b_bdn">
</div>
 
<div class="form-group">
    <label>email : </label>
    <input value="{{$biodata->email}}" disabled required type="text" class="form-control" name="email">
</div>
  

<div class="form-group">
    <label>Jumlah Saudara Kandung : </label>
    <input disabled value="{{$biodata->jml_sdr}}" type="number" class="form-control" name="jml_sdr">
</div>


<div class="form-group">
    <label>Jumlah Saudara Angkat : </label>
    <input disabled value="{{$biodata->jml_sdr_angkat}}" type="number" class="form-control" name="jml_sdr_angkat">
</div>
   

<div class="form-group">
    <label>Nama Ayah : </label>
    <input disabled value="{{$biodata->nm_ayah}}" type="text" class="form-control" name="nm_ayah">
</div>
 
<div class="form-group">
    <label>Tanggal Lahir Ayah : </label>
    <input disabled value="{{$biodata->ttl_ayah}}" type="date" class="form-control" name="ttl_ayah">
</div>
  

<div class="form-group">
    <label>Pendidikan Terahkir Ayah : </label>
    <input value="{{$biodata->pen_ayah}}" disabled type="text" class="form-control" name="pen_ayah">
</div>
 

<div class="form-group">
    <label>Pekerjaan Ayah : </label>
    <input value="{{$biodata->pek_ayah}}" disabled required type="text" class="form-control" name="pek_ayah">
</div>
   
<div class="form-group">
    <label for="gaji_ayah">Penghasilan Ayah : </label>
    <input value="{{$biodata->gaji_ayah}}" disabled type="text" class="form-control" name="gaji_ayah">
    
</div>
 

<div class="form-group">
    <label>Alamat Orang Tua : </label>
    <textarea disabled value="" class="form-control" name="alamat_ortu">{{$biodata->alamat_ortu}}</textarea>
</div>
   

<div class="form-group">
    <label>Masih Hidup / Meninggal Dunia : </label>
    <input disabled value="{{$biodata->hdp_mnggl}}" type="text" class="form-control" name="hdp_mnggl">
</div>
    

<div class="form-group">
    <label>No Hp Orang Tua : </label>
    <input disabled value="{{$biodata->hp_ortu}}" type="number" class="form-control" name="hp_ortu">
</div>
   

<div class="form-group">
    <label>Nama Ibu : </label>
    <input disabled value="{{$biodata->nm_ibu}}" type="text" class="form-control" name="nm_ibu">
</div>
   

<div class="form-group">
    <label>Tanggal Lahir Ibu : </label>
    <input disabled value="{{$biodata->ttl_ibu}}" type="date" class="form-control" name="ttl_ibu">
</div>
   

<div class="form-group">
    <label>Pendidikan Terakhir Ibu : </label>
    <input disabled value="{{$biodata->pen_ibu}}" type="text" class="form-control" name="pen_ibu">
</div>
 

<div class="form-group">
    <label>Pekerjaan Ibu : </label>
    <input value="{{$biodata->pek_ibu}}" disabled type="text" class="form-control" name="pek_ibu">
</div>
 

<div class="form-group">
    <label for="gaji_ibu">Penghasilan Ibu</label>
    <input value="{{$biodata->gaji_ibu}}" disabled type="text" class="form-control" name="gaji_ibu">
</div>
 
<div class="form-group">
    <label>Nama Wali : </label>
    <input disabled value="{{$biodata->nm_wali}}" type="text" class="form-control" name="nm_wali">
</div>


<div class="form-group">
    <label>Alamat Wali : </label>
    <textarea disabled value="" class="form-control" name="alamat_w">{{$biodata->alamat_w}}</textarea>
</div>
  

<div class="form-group">
    <label>No Hp Wali : </label>
    <input disabled value="{{$biodata->hp_wali}}" type="number" class="form-control" name="hp_wali">
</div>

{{-- 
<div class="form-group">
    <label>Foto Ijazah SD : </label>
    <img style="width:150px;height:200px;" src="{{asset('/ijazah-sd/'.$biodata->foto_ijazah_sd) }}" alt="" title="">
</div>

<div class="form-group">
    <label>Foto Ijazah SMP : </label>
    <img style="width:150px;height:200px;" src="{{asset('/ijazah-smp/'.$biodata->foto_ijazah_smp) }}" alt="" title="">
</div>

<div class="form-group">
    <label>Foto KK : </label>
    {{-- @php dd($biodata->foto_kk); @endphp --}}
    {{-- <img style="width:150px;height:200px;" src="{{asset('/foto-kk/'.$biodata->foto_kk) }}" alt="" title=""> --}}
{{-- </div> --}}

{{-- <div class="form-group">
    <label>Foto SKL : </label>
    <img style="width:150px;height:200px;" src="{{asset('/skl-smp/'.$biodata->foto_skl) }}" alt="" title="">
</div> --}}

<div class="form-group">
    <label>Foto Siswa : </label>
    <img style="width:150px;height:200px;" src="{{asset('/foto-siswa/'.$biodata->foto_siswa) }}" alt="" title="">
</div>

{{-- <p> {{$biodata->foto_kk}}</p><br> --}}
<a href="/biodata" class="btn btn-primary mb-3">Back</a>

@endsection 



  





