@extends('layout.layout_adminlte')

@section('judul')
Mohon Isi Sesuai dengan Kartu Keluarga  {{--INI BUAT JUDUL --}}
@endsection

{{-- ============Library Select 2================= --}}
@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
{{-- ============Library Select 2================= --}}


{{-- ============Library Tiny MCE================= --}}
@push('script')
    <script src="https://cdn.tiny.cloud/1/uk2mxtvj1fylr7xbwh1syguu8onyzd0vhcbsivjonobv9fwy/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
        selector: 'textarea',
        plugins: 'advlist autolink lists link image charmap preview anchor pagebreak',
        toolbar_mode: 'floating',
    });
    </script>
{{-- ============Library Tiny MCE================= --}}


{{-- ============Library Select 2================= --}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
      // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
        $('.js-example-basic-single').select2();
        });
    </script>
{{-- ============Library Select 2================= --}}

@endpush


@section('content')

<form action="/biodata" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama">
    </div>
        

    <div class="form-group">
        <label for="dit_kelas">Kelas</label>
        <select name="dit_kelas" class="js-example-basic-single" id="dit_kelas"><br>
            <option>--Pilih Salah Satu--</option>
                    {{-- @foreach ($biodata as $item)
                      <option value="{{$item->id}}">{{$item->dit_kelas}}</option>    
                    @endforeach --}}
            <option>X.01</option>
            <option>X.02</option>
            <option>X.03</option>
            <option>X.04</option>
            <option>X.05</option>
            <option>X.06</option>
            <option>X.07</option>
            <option>X.08</option>
            <option>X.09</option>
            <option>X.10</option>
            <option>X.11</option>
            <option>XI MIPA 1</option>
            <option>XI MIPA 2</option>
            <option>XI MIPA 3</option>
            <option>XI MIPA 4</option>
            <option>XI MIPA 5</option>
            <option>XI MIPA 6</option>
            <option>XI IPS 1</option>
            <option>XI IPS 2</option>
            <option>XI IPS 3</option>
            <option>XI IPS 4</option>
            <option>XII MIPA 1</option>
            <option>XII MIPA 2</option>
            <option>XII MIPA 3</option>
            <option>XII MIPA 4</option>
            <option>XII MIPA 5</option>
            <option>XII MIPA 6</option>
            <option>XII MIPA 7</option>
            <option>XII IPS 1</option>
            <option>XII IPS 2</option>
            <option>XII IPS 3</option>
        </select>
    </div>
     

    <div class="form-group">
        <label>Tanggal Diterima di SMA S AL KAUTSAR</label>
        <input type="date" class="form-control" name="pada_tgl">
    </div>
      
      
    <div class="form-group">
        <label>Semester</label>
        <input placeholder="Contoh : 1 (Satu)" type="text" class="form-control" name="semester">
    </div>
       

    <div class="form-group">
        <label for="jn_kelamin">Jenis Kelamin</label>
        <select name="jn_kelamin" class="js-example-basic-single" id="jn_kelamin">
            <option>--Pilih Salah Satu--</option>
            <option>Laki-Laki</option>
            <option>Perempuan</option>
        </select>
    </div>
      
        
    <div class="form-group">
        <label>NIK</label>
        <input required placeholder="16 digit" type="number" class="form-control" name="nik">
    </div>
       
    
    <div class="form-group">
        <label>No Kartu Keluarga</label>
        <input placeholder="16 digit"  type="number" class="form-control" name="no_kk">
    </div>
        

    <div class="form-group">
        <label>Tempat Lahir</label>
        <input  type="text" class="form-control" name="tm_lahir">
    </div>
     

    <div class="form-group">
        <label>Tanggal Lahir</label>
        <input  type="date" class="form-control" name="tgl_lahir">
    </div>
        

    <div class="form-group">
        <label>NISN</label>
        <input required  type="number" class="form-control" name="nisn">
    </div>
       
    <div class="form-group">
        <label>NIS</label>
        <input placeholder="4 Digit Angka" type="number" class="form-control" name="nis">
    </div>
       
    
    <div class="form-group">
        <label>Nomor Ijazah SMP</label>
        <input type="text" class="form-control" name="no_ijazah">
    </div>
      

    <div class="form-group">
        <label>Tahun Ijazah SMP</label>
        <input type="number" class="form-control" name="th_ijazah">
    </div>
        

    <div class="form-group">
        <label>No Seri SKHUN / SKL SMP</label>
        <input type="text" class="form-control" name="no_skhun">
    </div>
       

    <div class="form-group">
        <label>Tahun SKHUN / SKL SMP</label>
        <input type="number" class="form-control" name="th_skhun">
    </div>
        
    <div class="form-group">
        <label>Gol Darah</label>
        <input type="text" class="form-control" name="gol_darah">
    </div>
      

    <div class="form-group">
        <label>Agama</label>
        <input  type="text" class="form-control" name="agama">
    </div>
       

    <div class="form-group">
        <label>Kewarganegaraan</label>
        <input  type="text" class="form-control" name="kewargaan">
    </div>
      

    <div class="form-group">
        <label>Alamat Rumah</label>
        <textarea class="form-control" name="alamat_s"></textarea>
    </div>
        

    <div class="form-group">
        <label>Status Dalam Keluarga</label>
        <input Placeholder="Anak Kandung / Anak Sambung" type="text" class="form-control" name="st_dl_kel">
    </div>
       
    <div class="form-group">
        <label>No Hp</label>
        <input type="number" class="form-control" name="no_hp">
    </div>
     

    <div class="form-group">
        <label>Asal SMP / MTS</label>
        <input type="text" class="form-control" name="asal_smp">
    </div>
     

    <div class="form-group">
        <label>Asal Sekolah Pindahan</label>
        <input type="text" class="form-control" name="asal_pindahan">
    </div>
       

    <div class="form-group">
        <label>Jarak Rumah Kesekolah</label>
        <input placeholder="Ketik dalam Km" type="text" class="form-control" name="jrk_tinggal">
    </div>
    

    <div class="form-group">
        <label>Tinggi Badan</label>
        <input required placeholder="Contoh 172 cm" type="text" class="form-control" name="t_bdn">
    </div>
    

    <div class="form-group">
        <label>Berat Badan</label>
        <input required placeholder="Contoh 56 Kg" type="text" class="form-control" name="b_bdn">
    </div>
     
    <div class="form-group">
        <label>email</label>
        <input required type="text" class="form-control" name="email">
    </div>
      

    <div class="form-group">
        <label>Jumlah Saudara Kandung</label>
        <input type="number" class="form-control" name="jml_sdr">
    </div>
    

    <div class="form-group">
        <label>Jumlah Saudara Angkat</label>
        <input type="number" class="form-control" name="jml_sdr_angkat">
    </div>
       

    <div class="form-group">
        <label>Nama Ayah</label>
        <input type="text" class="form-control" name="nm_ayah">
    </div>
     
    <div class="form-group">
        <label>Tanggal Lahir Ayah</label>
        <input type="date" class="form-control" name="ttl_ayah">
    </div>
      

    <div class="form-group">
        <label>Pendidikan Terahkir Ayah</label>
        <input type="text" class="form-control" name="pen_ayah">
    </div>
     

    <div class="form-group">
        <label>Pekerjaan Ayah</label>
        <input required type="text" class="form-control" name="pek_ayah">
    </div>
       
    <div class="form-group">
        <label for="gaji_ayah">Penghasilan Ayah</label>
        <select name="gaji_ayah" class="js-example-basic-single" id="gaji_ayah">
            <option>--Pilih Salah Satu--</option>
            <option>Rp.1000.000 - Rp.2000.000</option>
            <option>Rp.2000.000 - Rp.4000.000</option>
            <option>Rp.4000.000 - Rp.8000.000</option>
            <option>Rp.8000.000 - Rp.20.000.000</option>
            <option> > Rp.20.000.000</option>
            <option>Tidak Berpenghasilan</option>
        </select>
    </div>
     

    <div class="form-group">
        <label>Alamat Orang Tua</label>
        <textarea class="form-control" name="alamat_ortu"></textarea>
    </div>
       
    
    <div class="form-group">
        <label>Masih Hidup / Meninggal Dunia</label>
        <input type="text" class="form-control" name="hdp_mnggl">
    </div>
        

    <div class="form-group">
        <label required>No Hp Orang Tua</label>
        <input type="number" class="form-control" name="hp_ortu">
    </div>
       

    <div class="form-group">
        <label>Nama Ibu</label>
        <input type="text" class="form-control" name="nm_ibu">
    </div>
       

    <div class="form-group">
        <label>Tanggal Lahir Ibu</label>
        <input type="date" class="form-control" name="ttl_ibu">
    </div>
       

    <div class="form-group">
        <label>Pendidikan Terakhir Ibu</label>
        <input type="text" class="form-control" name="pen_ibu">
    </div>
     

    <div class="form-group">
        <label>Pekerjaan Ibu</label>
        <input type="text" class="form-control" name="pek_ibu">
    </div>
     

    <div class="form-group">
        <label for="gaji_ibu">Penghasilan Ibu</label>
        <select name="gaji_ibu" class="js-example-basic-single" id="gaji_ibu">
            <option>--Pilih Salah Satu--</option>
            <option>Rp.1000.000 - Rp.2000.000</option>
            <option>Rp.2000.000 - Rp.4000.000</option>
            <option>Rp.4000.000 - Rp.8000.000</option>
            <option>Rp.8000.000 - Rp.20.000.000</option>
            <option> > Rp.20.000.000</option>
            <option>Tidak Berpenghasilan</option>
        </select>
    </div>
     
    <div class="form-group">
        <label>Nama Wali</label>
        <input type="text" class="form-control" name="nm_wali">
    </div>
   

    <div class="form-group">
        <label>Alamat Wali</label>
        <textarea class="form-control" name="alamat_w"></textarea>
    </div>
      

    <div class="form-group">
        <label>No Hp Wali</label>
        <input type="number" class="form-control" name="hp_wali">
    </div>
    

    <div class="form-group">
        <label>Foto Ijazah SD</label>
        <input type="file" class="form-control" name="foto_ijazah_sd">
    </div>
       

    <div class="form-group">
        <label>Foto Ijazah SMP/MTS</label>
        <input type="file" class="form-control" name="foto_ijazah_smp">
    </div>
   

    <div class="form-group">
        <label>Foto Kartu Keluarga</label>
        <input type="file" class="form-control" name="foto_kk">
    </div>
      

    <div class="form-group">
        <label>Foto SKL SMP/MTS</label>
        <input type="file" class="form-control" name="foto_skl">
    </div>
       

    <div class="form-group">
        <label>Foto Siswa</label>
        <input type="file" class="form-control" name="foto_siswa">
    </div>
     
            
 
    <button type="submit" class="btn btn-primary">Submit</button> <a href="/biodata" class="btn btn-primary">Cancel</a>
</form>

@endsection