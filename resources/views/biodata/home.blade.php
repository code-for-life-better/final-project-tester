@extends('layout.layout_adminlte')

@section('judul')
HALAMAN HOME    {{--INI BUAT JUDUL --}}
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>    
@endpush

@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush

@section('content')

<a href="/biodata/create" class="btn btn-success mb-3">Tambah Biodata</a>
        <table class="table" id="example1" class="table table-bordered table-striped">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Kelas</th>
                <th scope="col">Tempat Lahir</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">NISN</th>
                <th scope="col">NIS</th>
                <th scope="col">NIK</th>
                <th scope="col">Nomor HP</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($biodata as $key=>$value) <!--jika datanya ada isinya maka tampilkan yang ini-->
                    <tr>
                        <td>{{$key + 1}}</th>   {{-- supaya nomor di depan tampil 12345 --}}
                        <td>{{$value->nama}}</td> <!-- sesuaikan sama colom di database-->
                        <td>{{$value->dit_kelas}}</td>
                        <td>{{$value->tm_lahir}}</td>
                        <td>{{$value->tgl_lahir}}</td>
                        <td>{{$value->jn_kelamin}}</td>
                        <td>{{$value->nisn}}</td>
                        <td>{{$value->nis}}</td>
                        <td>{{$value->nik}}</td>
                        <td>{{$value->no_hp}}</td>
                        <td>
                            <form action="/biodata/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/biodata/{{$value->id}}" class="btn btn-info">Detail</a>
                                <a href="/biodata/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <input type="submit" onclick="return confirm('Are You Sure?')" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty <!--jika datanya isinya kosong maka tampilkan yang ini-->
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        {{$biodata->links()}}
@endsection