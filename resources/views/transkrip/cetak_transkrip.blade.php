<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">

<head>
  <title> Transkrip Siswa</title>
<link href="{{asset('css/transkrip/transcript_default.css')}}" rel="stylesheet" type="text/css" media="screen" title="Default" />
<link href="{{asset('css/transkrip/transcript_default_print.css')}}" rel="stylesheet" type="text/css" media="print" title="Default" />

<style type="text/css">
   .header_cetak tr td{
      font-family: Arial;
      padding: 0px;
   }
   .header_cetak tr td img{
      width: 70px;
      height: 70px;
      padding-bottom: 1px;
   }
   .header_cetak tr td h1{
      font-size: 17pt;
      margin-bottom: -3px;
   }
   .header_cetak tr td h3{
      font-size: 12pt;
      text-transform: uppercase;
   }
   .header_cetak tr td h4{
      font-size: 7pt;
      font-weight: normal;
      margin-top: -3px;
   }
   .header_cetak #top{
      padding-top: 3px;
      margin-bottom: -4px;
   }
   .header_cetak tr td h4 div{
      display: inline-flex;
   }
   .header_cetak tr td hr{
      border: solid 1px #000;
      margin-top: -1px;
   }

    .hr1{
        border: 1px solid #000 !important;
        margin-top: 0px !important;
    }
    .hr2{
        border: 0.6px solid #000 !important;
        margin-top: -5px !important;
    }
  </style>
  </head>
  <script>window.print();  </script>

<body>
  <div id="container">
   <div class="{BREAK}">
    <div id="header">

    <table {WIDTH} class="header_cetak">
      <tr>
         <td><img src="https://tse3.mm.bing.net/th?id=OIP.OE94v-wcUq4keiBV0YRlGQAAAA&pid=Api&P=0"/></td>
         <td width="100%">
            <center>
                <h1>SEKOLAH MENENGAH ATAS BANDAR LAMPUNG</h1>
                <h4 id="top">
                   <div>Alamat : Jl. Soekarno - Hatta, Rajabasa, Kota Bandar Lampung, Lampung 35144, Indonesia., </div>
                   <div>Telp : (0721) 781578,</div>
                </h4>
                <h4 style="line-height: 25px;">
                   <div>Website : https://alkautsarlampung.sch.id/ &nbsp;&nbsp;</div>
                   <div>Email : perguruanalkautsar@gmail.com</div>
                </h4>
            </center>
         </td>
      </tr>
      <tr>
         <td colspan="3"><hr class="hr1"><hr class="hr2"></td>
      </tr>
   </table>

<br>
   <h3 style="font-family: arial;">Transkrip Nilai</h3><br>


<div id="info_siswa">
      <table width="100%" border="0">
        <tr>
          <th rowspan="5" width="4%">&nbsp;</th>
          <th width="15%">Nama</th>
          <th width="1%">:</th>
          <td width="32%">{{$dataSiswa->nama}}</td>
          <th width="15%">NIS</th>
          <th width="1%">:</th>
          <td width="32%">{{$dataSiswa->nis}}</td>
        </tr>
        <tr>
          <th nowrap="nowrap">Tempat / Tanggal Lahir</th>
          <th>:</th>
          <td>{{$dataSiswa->tm_lahir}} / {{$dataSiswa->tgl_lahir}}</td>
          <th>No Ijazah</th>
          <th>:</th>
          <td>{{$dataSiswa->no_ijazah}}</td>
        </tr>
        <tr>
          <th nowrap="nowrap">No Transkrip</th>
          <th>:</th>
          <td>{{$transkrip->nomor_transkrip}}</td>
          <th>Tanggal Transkrip</th>
          <th>:</th>
          <td>{{$transkrip->tanggal_transkrip}}</td>
        </tr>
      </table>
    </div>

<div id="table_transkrip">
<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Mata Pelajaran</th>
        <th scope="col">Nilai</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($arrNilai as $key => $item)
        <tr>
            <td >{{$key + 1}}</td>
            <td>{{$item->kode_mapel}} - {{$item->nama_mapel}}</td>
            <td>{{$item->nilai}}</td>
        </tr>
        @empty
            <h2>Data tidak ada</h2>
        @endforelse
    </tbody>
  </table>
</div>
  </div>
  </div>
</body>

</html>
