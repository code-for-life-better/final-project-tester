<script type="text/javascript" language="javascript">
  const validasi = () => {

      alert("Data Transkrip Berhasil di simpan");
      return true;
  }
</script>
@extends('layout.layout_adminlte')

@section('judul')
	Transkrip Nilai {{$dataSiswa->nama}}
@endsection

@section('content')
<form method="post" name="frmInput" onsubmit="return validasi()" action="/transkrip/{{$transkrip->id}}/{{$dataSiswa->id}}">
    @csrf
    @method('PUT')
    <div class="form-group">
      {{-- tampilkan disini relasi one to many nya $transkrip->nama fungsi di model transkrip->nama tabelnya
      <label>Nama Siswa</label><br />
      <input type="text" disabled name="nama" value="{{$transkrip->biodata->nama}}" class="form-control" >
    </div> --}}

      <label>Nomor Transkrip</label><br />
      <input type="text" name="nomor_transkrip" value="{{$transkrip->nomor_transkrip}}" class="form-control" >
    </div>
    <div class="form-group">
      <label>Tanggal Transkrip</label>
      <input type="date" name="tanggal_transkrip" value="{{$transkrip->tanggal_transkrip}}" class="form-control" >
    </div>
    <div style="text-align: center;">
    <input type="submit" class="btn btn-primary" value="Simpan">
    <a href="/cetak_transkrip/{{$dataSiswa->id}}" target="_blank" class="btn btn-success">Cetak Transkrip</a>
    </div>
  </form>
  <br />

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Mata Pelajaran</th>
        <th scope="col">Nilai</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($arrNilai as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->kode_mapel}} - {{$item->nama_mapel}}</td>
            <td>{{$item->nilai}}</td>
        </tr>
        @empty
            <h2>Data tidak ada</h2>
        @endforelse
    </tbody>
  </table>
@endsection
