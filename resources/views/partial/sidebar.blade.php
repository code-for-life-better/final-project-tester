<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin/index3.html')}}" class="brand-link">
      <img src="{{url('admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><strong>KESISWAAN</strong></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Hi !{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                
              </p>
            </a>
            <li class="nav-item">
              <a href="/biodata" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Data Siswa
                </p>
              </a>
            </li>
          
            <li class="nav-item">
              <a href="/mapel" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>
                  Data Matapelajaran
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="/guru" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Data Guru
                </p>
              </a>
            </li>
             <li class="nav-item">
              <a href="/nilai_daftar_siswa" class="nav-link">
                <i class="nav-icon fas fa-file"></i>
                <p>
                  Input Nilai
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="/transkrip" class="nav-link">
                <i class="nav-icon fas fa-file"></i>
                <p>
                  Transkrip Nilai
                </p>
              </a>
            </li>

            <li class="nav-item">
             
                    <a class="nav-link bg-danger" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                    
                    <i class="nav-icon fas fa-sign-out" aria-hidden="true"></i>
                    <p>logout</p>
                    {{-- {{ __('Logout') }} --}}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>
               
            </li>
         
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>