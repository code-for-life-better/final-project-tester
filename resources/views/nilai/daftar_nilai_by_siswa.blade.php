@extends('layout.layout_adminlte')

@section('judul')
	Daftar Nilai {{$dataSiswa->nama}}
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>    
@endpush

@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush


@section('content')
<a class="btn btn-success mb-3" href="/nilai/{{$dataSiswa->id}}/nilai_input_nilai">Tambah Nilai</a>
<table id="example1" class="table table-bordered table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Mata Pelajaran</th>
        <th scope="col">Nilai</th>
        <th scope="col">Aksi </th>
      </tr>
    </thead>
    <tbody>
        @forelse ($arrNilai as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->kode_mapel}} - {{$item->nama_mapel}}</td>
            <td>{{$item->nilai}}</td>
            <td>
                <form action="/nilai/{{$item->id}}/{{$dataSiswa->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                <a href="/nilai_edit/{{$item->id}}/{{$dataSiswa->id}}" class="btn btn-success btn-sm">Edit </a>
                </form>
            </td>
        </tr>
        @empty
            <h2>Data tidak ada</h2>
        @endforelse
    </tbody>
  </table>
@endsection
