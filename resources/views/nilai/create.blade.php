<script type="text/javascript" language="javascript">
  const validasi = () => {
    if (document.forms["frmInput"]["kode"].value == "") {
      alert("Kode Mata Pelajaran belum di isi");
      document.forms["frmInput"]["kode"].focus();
      return false;
    }

    if (document.forms["frmInput"]["nama"].value == "") {
      alert("Nama Mata Pelajaran belum di isi");
      document.forms["frmInput"]["nama"].focus();
      return false;
    }
  }
</script>
@extends('layout.layout_adminlte')

@section('judul')
	Tambah Matapelajaran
@endsection

@section('content')
<form method="post" name="frmInput" onsubmit="return validasi()" action="/mapel">
    @csrf
    <div class="form-group">
      <label>Kode Mata Pelajaran</label>
      <input type="text" name="kode" value="" class="form-control" >
    </div>
    <div class="form-group">
      <label>Nama Mata Pelajaran</label>
      <input type="text" name="nama" value="" class="form-control" >
    </div>
    <input type="submit" class="btn btn-primary" value="Simpan">
  </form>
@endsection