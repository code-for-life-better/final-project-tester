<script type="text/javascript" language="javascript">
  const validasi = () => {
    if (document.forms["frmInput"]["nilai"].value == "") {
      alert("Nilai belum di isi");
      document.forms["frmInput"]["nilai"].focus();
      return false;
    }

    if (document.forms["frmInput"]["matapelajaran_id"].value == "") {
      alert("Mata Pelajaran belum di pilih");
      document.forms["frmInput"]["matapelajaran_id"].focus();
      return false;
    }
  }
</script>
@extends('layout.layout_adminlte')

@section('judul')
	Edit Nilai
@endsection

@section('content')
<form method="post" name="frmInput" onsubmit="return validasi()" action="/nilai_update/{{$nilai->id}}">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Mata Pelajaran</label><br />
       <select  id="" name="matapelajaran_id" class="js-example-basic-single form-control">
          <option value="">-----Pilih Mata Pelajaran-----</option>
          @foreach ($matapelajaran as $item)
            <option value="{{$item->id}}">{{$item->kode_mapel}} - {{$item->nama_mapel}}</option>    
          @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Nilai</label>
      <input type="text" name="nilai" value="{{$nilai->nilai}}" class="form-control" >
    </div>
    <input type="submit" class="btn btn-primary" value="Simpan">
  </form>
@endsection