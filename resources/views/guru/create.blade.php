
@extends('layout.layout_adminlte')

@section('judul')
	Tambah Guru
@endsection

@section('content')
<form method="POST" name="frmInput"  action="/guru">
    @csrf

    <div class="form-group">
      <label>Mata Pelajaran</label>
      <select  id="" name="matapelajaran_id" class="js-example-basic-single form-control">
          <option value="">-----Pilih Mata Pelajaran-----</option>
          @foreach ($matapelajaran as $item)
            <option value="{{$item->id}}">{{$item->kode_mapel}} - {{$item->nama_mapel}}</option>    
          @endforeach
      </select>

    </div>

    <div class="form-group">
      <label>Nama Guru</label>
      <input type="text" required name="nama" value="" class="form-control" >
    </div>
    <div class="form-group">
      <label>Golongan</label>
      <select name="golongan" class="form-control" id="golongan">
        <option value="">--Pilih Salah Satu--</option>
        <option>III/a</option>
        <option>III/b</option>
        <option>III/c</option>
        <option>III/d</option>
        <option>IV/a</option>
        <option>IV/b</option>
    </select>
    </div>
    <div class="form-group">
      <label>Tempat Lahir</label>
      <input required type="text" name="tm_lahir" value="" class="form-control" >
    </div>
    <div class="form-group">
      <label>Tanggal Lahir</label>
      <input required type="date" name="tgl_lahir" value="" class="form-control" >
    </div>
    <div class="form-group">
      <label>NIK</label>
      <input required type="number" name="nik" value="" class="form-control" >
    </div>


    <input type="submit" class="btn btn-primary" value="Simpan"> 
    <a href="/guru" class="btn btn-primary">Cancel</a>
  </form>
@endsection