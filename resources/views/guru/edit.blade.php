
@extends('layout.layout_adminlte')

@section('judul')
	Ubah Guru
@endsection

@section('content')
<form method="post" name="frmInput" action="/guru/{{$guru->id}}">
    @csrf
    @method('PUT')

    <div class="form-group">
      <label>Kode Mapel</label>
      <select  id="" class="js-example-basic-single">
          <option value="">-----pilih kode mapel-----</option>
          @foreach ($matapelajaran as $item)
            <option value="{{$item->id}}"selected>{{$item->kode_mapel}} - {{$item->nama_mapel}}</option>    
          @endforeach
      </select>

    </div>

    <div class="form-group">
      <label>Nama Guru</label>
      <input type="text" required name="nama" value="" class="form-control" >
    </div>
    <div class="form-group">
      <label>Golongan</label>
      <select name="golongan" class="form-control" id="golongan">
        <option value="">--Pilih Salah Satu--</option>
        <option value="{{$guru->golongan}}"selected>{{$guru->golongan}}</option>
        <option>III/a</option>
        <option>III/b</option>
        <option>III/c</option>
        <option>III/d</option>
        <option>IV/a</option>
        <option>IV/b</option>
    </select>
    </div>
    <div class="form-group">
      <label>Tempat Lahir</label>
      <input {{$guru->'tm_lahir'}} required type="text" name="tm_lahir" value="" class="form-control" >
    </div>
    <div class="form-group">
      <label>Tanggal Lahir</label>
      <input {{$guru->'tgl_lahir'}} required type="date" name="tgl_lahir" value="" class="form-control" >
    </div>
    <div class="form-group">
      <label>NIK</label>
      <input {{$guru->'nik'}} required type="number" name="nik" value="" class="form-control" >
    </div>


    <button type="submit" class="btn btn-primary mr-2" >Update</button> <a href="/guru" class="btn btn-primary">Cancel</a>
  </form>
@endsection