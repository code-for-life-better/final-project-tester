<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{

    protected $table = 'guru';

    protected $fillable = ['nama', 'golongan', 'tm_lahir', 'tgl_lahir', 'nik', 'matapelajaran_id'];


    public function mapel()
    {
        return $this->belongsTo('App\Mapel');
    }
}
