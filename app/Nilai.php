<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    //
    protected $table = 'nilai';

    protected $fillable = ['matapelajaran_id', 'transkrip_id', 'nilai'];

    public function mapel()
    {
        return $this->belongsToMany('App\Mapel');
    }
    public function transkrip()
    {
        return $this->belongsToMany('App\Transkrip');
    }
}
