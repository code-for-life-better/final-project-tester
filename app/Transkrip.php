<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transkrip extends Model
{
    //
    protected $table = 'transkrip';

    protected $fillable = ['nomor_transkrip', 'tanggal_trankrip', 'id_biodata'];


    public function biodata()
    {
        return $this->belongsTo('App\Biodata');
    }

    public function nilai()
    {
        return $this->hasMany('App\Nilai');
    }
}
