<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nilai;
use App\Transkrip;
use App\Biodata;
use DB;
use RealRashid\SweetAlert\Facades\Alert;



class TranskripController extends Controller
{
    //
    public function NilaiDaftarSiswa()
    {
        $siswa = DB::table('biodata')
        ->join('transkrip', 'biodata.id', '=', 'id_biodata')
        ->select('biodata.*')
            ->get();
        return view('transkrip.index', compact('siswa'));
    }

    public function DaftarNilaiBySiswa($siswa_id)
    {
        $dataSiswa = DB::table('biodata')
        ->where('biodata.id', '=', $siswa_id)
            ->get()[0];

        $arrTranskrip = DB::table('transkrip')
            ->where('transkrip.id_biodata', '=', $siswa_id)
            ->get();

        $transkrip = $arrTranskrip[0];

        $arrNilai = DB::table('nilai')
            ->leftJoin('matapelajaran', 'matapelajaran.id', '=', 'matapelajaran_id')
            ->where('nilai.transkrip_id', '=', $transkrip->id)
            ->select('nilai.*', 'nama_mapel', 'kode_mapel')
            ->get();

        return view('transkrip.daftar_nilai_by_siswa', compact('dataSiswa', 'transkrip', 'arrNilai'));
    }

    public function update(Request $request, $transkrip_id, $siswa_id){

        $transkrip = Transkrip::find($transkrip_id);

        // var_dump($transkrip);die;
        $transkrip->nomor_transkrip = $request->nomor_transkrip;
        $transkrip->tanggal_transkrip = $request->tanggal_transkrip;
        
        $transkrip->save();

        return redirect('/transkrip/'.$siswa_id.'/daftar_nilai_by_siswa');
    }


    public function CetakTranskrip($siswa_id){
        $dataSiswa = DB::table('biodata')
            ->get()[0];

        $arrTranskrip = DB::table('transkrip')
            ->where('transkrip.id_biodata', '=', $siswa_id)
            ->get();

        $transkrip = $arrTranskrip[0];

        $arrNilai = DB::table('nilai')
            ->leftJoin('matapelajaran', 'matapelajaran.id', '=', 'matapelajaran_id')
            ->where('nilai.transkrip_id', '=', $transkrip->id)
            ->select('nilai.*', 'nama_mapel', 'kode_mapel')
            ->get();

        return view('transkrip.cetak_transkrip', compact('dataSiswa', 'transkrip', 'arrNilai'));
    }
}
