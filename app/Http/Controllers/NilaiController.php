<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nilai;
use App\Transkrip;
use App\Biodata;
use DB;
use RealRashid\SweetAlert\Facades\Alert;



class NilaiController extends Controller
{
    //
    public function NilaiDaftarSiswa()
    {
        $siswa = DB::table('biodata')
            ->get();
        return view('nilai.index', compact('siswa'));
    }

    public function DaftarNilaiBySiswa($siswa_id)
    {
        $dataSiswa = DB::table('biodata')
         ->where('biodata.id', '=', $siswa_id)
            ->get()[0];

        $arrTranskrip = DB::table('transkrip')
            ->where('transkrip.id_biodata', '=', $siswa_id)
            ->get();

        if (count($arrTranskrip) == 0) {
            $transkrip = new Transkrip;
            $transkrip->id_biodata = $siswa_id;
            $transkrip->save();

            $arrTranskrip = DB::table('transkrip')
                ->where('transkrip.id_biodata', '=', $siswa_id)
                ->get();
        }

        $transkrip = $arrTranskrip[0];

        $arrNilai = DB::table('nilai')
            ->leftJoin('matapelajaran', 'matapelajaran.id', '=', 'matapelajaran_id')
            ->where('nilai.transkrip_id', '=', $transkrip->id)
            ->select('nilai.*', 'nama_mapel', 'kode_mapel')
            ->get();

        return view('nilai.daftar_nilai_by_siswa', compact('dataSiswa', 'transkrip', 'arrNilai'));
    }

    public function InputNilai($siswa_id)
    {
        $arrTranskrip = DB::table('transkrip')
            ->where('transkrip.id_biodata', '=', $siswa_id)
            ->get();

        if (count($arrTranskrip) == 0) {
            $transkrip = new Transkrip;
            $transkrip->id_biodata = $siswa_id;
            $transkrip->save();

            $arrTranskrip = DB::table('transkrip')
                ->where('transkrip.id_biodata', '=', $siswa_id)
                ->get();
        }

        $transkrip = $arrTranskrip[0];

        $matapelajaran = DB::table('matapelajaran')->get();

        return view('nilai.input_nilai', compact('transkrip', 'matapelajaran'));
    }


    public function store(Request $request, $siswa_id)
    {
        $nilai = new Nilai;

        $nilai->matapelajaran_id = $request->matapelajaran_id;
        $nilai->transkrip_id = $request->transkrip_id;
        $nilai->nilai = $request->nilai;

        $nilai->save();

        Alert::success('Success', 'Data Berhasil Ditambah');
        return redirect('/nilai/' . $siswa_id . '/daftar_nilai_by_siswa');
    }


    public function destroy($nilai_id, $siswa_id)
    {
        $nilai = Nilai::find($nilai_id);
        $nilai->delete();

        Alert::info('Info Title', 'Berhasil Dihapus');
        return redirect('/nilai/' . $siswa_id . '/daftar_nilai_by_siswa');
    }

    public function edit($nilai_id)
    {
        $nilai = Nilai::where('id', $nilai_id)->first();
        $matapelajaran = DB::table('matapelajaran')->get();
        return view('nilai.edit_nilai', compact('nilai', 'matapelajaran'));
    }


    public function update(Request $request, $nilai_id)
    {
        $nilai = Nilai::find($nilai_id);

        $nilai->matapelajaran_id = $request->matapelajaran_id;
        $nilai->nilai = $request->nilai;

        $siswa = DB::table('nilai')
            ->leftJoin('transkrip', 'transkrip.id', '=', 'transkrip_id')
            ->leftJoin('biodata', 'biodata.id', '=', 'transkrip.id_biodata')
            ->where('nilai.id', '=', $nilai_id)
            ->select('biodata.id')
            ->get()[0];

        $nilai->save();

        Alert::success('Success', 'Berhasil diupdate');
        return redirect('/nilai/' . $siswa->id . '/daftar_nilai_by_siswa');
    }
}
