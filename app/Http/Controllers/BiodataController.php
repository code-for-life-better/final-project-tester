<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biodata;
use DB;
use File;
use RealRashid\SweetAlert\Facades\Alert;


class BiodataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $biodata = Biodata::paginate(3);
        //$biodata = Biodata::all();
        return view('biodata.home', compact('biodata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('biodata.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'dit_kelas' => 'required',
            'pada_tgl' => 'required',
            'semester' => 'required',
            'jn_kelamin' => 'required',
            'nik' => 'required',
            'no_kk' => 'required',
            'tm_lahir' => 'required',
            'tgl_lahir' => 'required',
            'nisn' => 'required',
            'nis' => 'nullable',
            'no_ijazah' => 'nullable',
            'th_ijazah' => 'nullable',
            'no_skhun' => 'nullable',
            'th_skhun' => 'nullable',
            'gol_darah' => 'nullable',
            'agama' => 'required',
            'kewargaan' => 'required',
            'alamat_s' => 'required',
            'st_dl_kel' => 'required',
            'no_hp' => 'required',
            'asal_smp' => 'required',
            'asal_pindahan' => 'nullable',
            'jrk_tinggal' => 'required',
            't_bdn' => 'required',
            'b_bdn' => 'required',
            'email' => 'required',
            'jml_sdr' => 'required',
            'jml_sdr_angkat' => 'nullable',
            'nm_ayah' => 'required',
            'ttl_ayah' => 'required',
            'pen_ayah' => 'required',
            'pek_ayah' => 'required',
            'gaji_ayah' => 'required',
            'alamat_ortu' => 'required',
            'hdp_mnggl' => 'required',
            'hp_ortu' => 'required',
            'nm_ibu' => 'required',
            'ttl_ibu' => 'required',
            'pen_ibu' => 'required',
            'pek_ibu' => 'required',
            'gaji_ibu' => 'required',
            'nm_wali' => 'nullable',
            'alamat_w' => 'nullable',
            'hp_wali' => 'nullable',
            'foto_ijazah_sd' => 'image|mimes:jpeg,png,jpg',
            'foto_ijazah_smp' => 'image|mimes:jpeg,png,jpg',
            'foto_kk' => 'image|mimes:jpeg,png,jpg',
            'foto_skl' => 'image|mimes:jpeg,png,jpg',
            'foto_siswa' => 'image|mimes:jpeg,png,jpg'
        ]);

        $nm_ijazah_sd = $request->nis . '-' . $request->nama . '.' . $request->foto_ijazah_sd->extension();
        $request->foto_ijazah_sd->move(public_path('ijazah-sd'), $nm_ijazah_sd);

        $nm_ijazah_smp = $request->nis . '-' . $request->nama . '.' . $request->foto_ijazah_smp->extension();
        $request->foto_ijazah_smp->move(public_path('ijazah-smp'), $nm_ijazah_smp);

        $nm_kk = $request->nis . '-' . $request->nama . '.' . $request->foto_kk->extension();
        $request->foto_kk->move(public_path('foto-kk'), $nm_kk);

        $nm_skl = $request->nis . '-' . $request->nama . '.' . $request->foto_skl->extension();
        $request->foto_skl->move(public_path('skl-smp'), $nm_skl);

        $nm_siswa = $request->nis . '-' . $request->nama . '.' . $request->foto_siswa->extension();
        $request->foto_siswa->move(public_path('foto-siswa'), $nm_siswa);


        $biodata = new Biodata;

        $biodata->nama = $request->nama;
        $biodata->dit_kelas = $request->dit_kelas;
        $biodata->pada_tgl = $request->pada_tgl;
        $biodata->semester = $request->semester;
        $biodata->jn_kelamin = $request->jn_kelamin;
        $biodata->nik = $request->nik;
        $biodata->no_kk = $request->no_kk;
        $biodata->tm_lahir = $request->tm_lahir;
        $biodata->tgl_lahir = $request->tgl_lahir;
        $biodata->nisn = $request->nisn;
        $biodata->nis = $request->nis;
        $biodata->no_ijazah = $request->no_ijazah;
        $biodata->th_ijazah = $request->th_ijazah;
        $biodata->no_skhun = $request->no_skhun;
        $biodata->th_skhun = $request->th_skhun;
        $biodata->gol_darah = $request->gol_darah;
        $biodata->agama = $request->agama;
        $biodata->kewargaan = $request->kewargaan;
        $biodata->alamat_s = $request->alamat_s;
        $biodata->st_dl_kel = $request->st_dl_kel;
        $biodata->no_hp = $request->no_hp;
        $biodata->asal_smp = $request->asal_smp;
        $biodata->asal_pindahan = $request->asal_pindahan;
        $biodata->jrk_tinggal = $request->jrk_tinggal;
        $biodata->t_bdn = $request->t_bdn;
        $biodata->b_bdn = $request->b_bdn;
        $biodata->email = $request->email;
        $biodata->jml_sdr = $request->jml_sdr;
        $biodata->jml_sdr_angkat = $request->jml_sdr_angkat;
        $biodata->nm_ayah = $request->nm_ayah;
        $biodata->ttl_ayah = $request->ttl_ayah;
        $biodata->pen_ayah = $request->pen_ayah;
        $biodata->pek_ayah = $request->pek_ayah;
        $biodata->gaji_ayah = $request->gaji_ayah;
        $biodata->alamat_ortu = $request->alamat_ortu;
        $biodata->hdp_mnggl = $request->hdp_mnggl;
        $biodata->hp_ortu = $request->hp_ortu;
        $biodata->nm_ibu = $request->nm_ibu;
        $biodata->ttl_ibu = $request->ttl_ibu;
        $biodata->pen_ibu = $request->pen_ibu;
        $biodata->pek_ibu = $request->pek_ibu;
        $biodata->gaji_ibu = $request->gaji_ibu;
        $biodata->nm_wali = $request->nm_wali;
        $biodata->alamat_w = $request->alamat_w;
        $biodata->hp_wali = $request->hp_wali;
        //foto
        $biodata->foto_ijazah_sd = $nm_ijazah_sd;
        $biodata->foto_ijazah_smp = $nm_ijazah_smp;
        $biodata->foto_kk = $nm_kk;
        $biodata->foto_skl = $nm_skl;
        $biodata->foto_siswa = $nm_siswa;

        $biodata->save();

        Alert::success('Success', 'Data Siswa Berhasil Disimpan');
        return redirect('/biodata/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($biodata_id)
    {
        $biodata = Biodata::where('id', $biodata_id)->first();
        return view('biodata.show', compact('biodata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($biodata_id)
    {
        $biodata = Biodata::where('id', $biodata_id)->first();
        return view('biodata.edit', compact('biodata'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $biodata_id)
    {
        $request->validate([
            'nama' => 'required',
            'dit_kelas' => 'required',
            'pada_tgl' => 'required',
            'semester' => 'required',
            'jn_kelamin' => 'required',
            'nik' => 'required',
            'no_kk' => 'required',
            'tm_lahir' => 'required',
            'tgl_lahir' => 'required',
            'nisn' => 'required',
            'nis' => 'nullable',
            'no_ijazah' => 'nullable',
            'th_ijazah' => 'nullable',
            'no_skhun' => 'nullable',
            'th_skhun' => 'nullable',
            'gol_darah' => 'nullable',
            'agama' => 'required',
            'kewargaan' => 'required',
            'alamat_s' => 'required',
            'st_dl_kel' => 'required',
            'no_hp' => 'required',
            'asal_smp' => 'required',
            'asal_pindahan' => 'nullable',
            'jrk_tinggal' => 'required',
            't_bdn' => 'required',
            'b_bdn' => 'required',
            'email' => 'required',
            'jml_sdr' => 'required',
            'jml_sdr_angkat' => 'nullable',
            'nm_ayah' => 'required',
            'ttl_ayah' => 'required',
            'pen_ayah' => 'required',
            'pek_ayah' => 'required',
            'gaji_ayah' => 'required',
            'alamat_ortu' => 'required',
            'hdp_mnggl' => 'required',
            'hp_ortu' => 'required',
            'nm_ibu' => 'required',
            'ttl_ibu' => 'required',
            'pen_ibu' => 'required',
            'pek_ibu' => 'required',
            'gaji_ibu' => 'required',
            'nm_wali' => 'nullable',
            'alamat_w' => 'nullable',
            'hp_wali' => 'nullable',
            'foto_ijazah_sd' => 'image|mimes:jpeg,png,jpg',
            'foto_ijazah_smp' => 'image|mimes:jpeg,png,jpg',
            'foto_kk' => 'image|mimes:jpeg,png,jpg',
            'foto_skl' => 'image|mimes:jpeg,png,jpg',
            'foto_siswa' => 'image|mimes:jpeg,png,jpg'
        ]);

        $nm_ijazah_sd = $request->nis . '-' . $request->nama . '.' . $request->foto_ijazah_sd->extension();
        $request->foto_ijazah_sd->move(public_path('ijazah-sd'), $nm_ijazah_sd);

        $nm_ijazah_smp = $request->nis . '-' . $request->nama . '.' . $request->foto_ijazah_smp->extension();
        $request->foto_ijazah_smp->move(public_path('ijazah-smp'), $nm_ijazah_smp);

        $nm_kk = $request->nis . '-' . $request->nama . '.' . $request->foto_kk->extension();
        $request->foto_kk->move(public_path('foto-kk'), $nm_kk);

        $nm_skl = $request->nis . '-' . $request->nama . '.' . $request->foto_skl->extension();
        $request->foto_skl->move(public_path('skl-smp'), $nm_skl);

        $nm_siswa = $request->nis . '-' . $request->nama . '.' . $request->foto_siswa->extension();
        $request->foto_siswa->move(public_path('foto-siswa'), $nm_siswa);

        $biodata = Biodata::find($biodata_id);

        $biodata->nama = $request->nama;
        $biodata->dit_kelas = $request->dit_kelas;
        $biodata->pada_tgl = $request->pada_tgl;
        $biodata->semester = $request->semester;
        $biodata->jn_kelamin = $request->jn_kelamin;
        $biodata->nik = $request->nik;
        $biodata->no_kk = $request->no_kk;
        $biodata->tm_lahir = $request->tm_lahir;
        $biodata->tgl_lahir = $request->tgl_lahir;
        $biodata->nisn = $request->nisn;
        $biodata->nis = $request->nis;
        $biodata->no_ijazah = $request->no_ijazah;
        $biodata->th_ijazah = $request->th_ijazah;
        $biodata->no_skhun = $request->no_skhun;
        $biodata->th_skhun = $request->th_skhun;
        $biodata->gol_darah = $request->gol_darah;
        $biodata->agama = $request->agama;
        $biodata->kewargaan = $request->kewargaan;
        $biodata->alamat_s = $request->alamat_s;
        $biodata->st_dl_kel = $request->st_dl_kel;
        $biodata->no_hp = $request->no_hp;
        $biodata->asal_smp = $request->asal_smp;
        $biodata->asal_pindahan = $request->asal_pindahan;
        $biodata->jrk_tinggal = $request->jrk_tinggal;
        $biodata->t_bdn = $request->t_bdn;
        $biodata->b_bdn = $request->b_bdn;
        $biodata->email = $request->email;
        $biodata->jml_sdr = $request->jml_sdr;
        $biodata->jml_sdr_angkat = $request->jml_sdr_angkat;
        $biodata->nm_ayah = $request->nm_ayah;
        $biodata->ttl_ayah = $request->ttl_ayah;
        $biodata->pen_ayah = $request->pen_ayah;
        $biodata->pek_ayah = $request->pek_ayah;
        $biodata->gaji_ayah = $request->gaji_ayah;
        $biodata->alamat_ortu = $request->alamat_ortu;
        $biodata->hdp_mnggl = $request->hdp_mnggl;
        $biodata->hp_ortu = $request->hp_ortu;
        $biodata->nm_ibu = $request->nm_ibu;
        $biodata->ttl_ibu = $request->ttl_ibu;
        $biodata->pen_ibu = $request->pen_ibu;
        $biodata->pek_ibu = $request->pek_ibu;
        $biodata->gaji_ibu = $request->gaji_ibu;
        $biodata->nm_wali = $request->nm_wali;
        $biodata->alamat_w = $request->alamat_w;
        $biodata->hp_wali = $request->hp_wali;
        //foto
        $biodata->foto_ijazah_sd = $nm_ijazah_sd;
        $biodata->foto_ijazah_smp = $nm_ijazah_smp;
        $biodata->foto_kk = $nm_kk;
        $biodata->foto_skl = $nm_skl;
        $biodata->foto_siswa = $nm_siswa;

        $biodata->save();

        Alert::success('Success', 'Update Berhasil');
        return redirect('/biodata');

        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($biodata_id)
    {
        $biodata = Biodata::find($biodata_id);

        $biodata->delete();

        Alert::info('Info', 'Data Berhasil dihapus');
        return redirect('/biodata');
    }
}
