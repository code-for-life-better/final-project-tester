<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminlteController extends Controller
{
    public function adminlte()
    {
        return view('layout.layout_adminlte');
    }
}
