<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guru;
use DB;
use RealRashid\SweetAlert\Facades\Alert;


class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $guru = Guru::paginate(3);
        $guru = DB::table('guru')
            ->leftJoin('matapelajaran', 'matapelajaran_id', '=', 'matapelajaran.id')
            ->get();
        //$biodata = Biodata::all();
            

        return view('guru.index', compact('guru'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $matapelajaran = DB::table('matapelajaran')->get();
        return view('guru.create', compact('matapelajaran'));
        // return view('guru.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'nama' => 'required',
        //     'golongan' => 'required',
        //     'tm_lahir' => 'required',
        //     'tgl_lahir' => 'required',
        //     'nik' => 'required',
        //     'matapelajaran_id' => 'required'
        // ]);
        $guru = new Guru;

        $guru->nama = $request->nama;
        $guru->golongan = $request->golongan;
        $guru->tm_lahir = $request->tm_lahir;
        $guru->tgl_lahir = $request->tgl_lahir;
        $guru->nik = $request->nik;
        $guru->matapelajaran_id = $request->matapelajaran_id;


        $guru->save();

        Alert::success('Success', 'Data berhasil ditambah');
        return redirect('/guru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($guru_id)
    {
        $guru = Guru::where('id', $guru_id)->first();
        return view('guru.show', compact('guru'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($guru_id)
    {
        $guru = Guru::where('id', $guru_id)->first();
        return view('guru.edit', compact('guru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $guru_id)
    {
        $request->validate([
            'nama' => 'required',
            'golongan' => 'required',
            'tm_lahir' => 'required',
            'tgl_lahir' => 'required',
            'nik' => 'required',
            'matapelajaran_id' => 'required'
        ]);

        $guru = Guru::find($guru_id);;

        $guru->nama = $request->nama;
        $guru->golongan = $request->golongan;
        $guru->tm_lahir = $request->tm_lahir;
        $guru->tgl_lahir = $request->tgl_lahir;
        $guru->nik = $request->nik;
        $guru->matapelajaran_id = $request->matapelajaran_id;


        $guru->save();

        Alert::info('Info', 'Update Berhasil');
        return redirect('/guru');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($guru_id)
    {
        $biodata = Guru::find($guru_id);

        $biodata->delete();

        Alert::info('Info', 'Berhasil dihapus');
        return redirect('/guru');
    }
}
