<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mapel;
use RealRashid\SweetAlert\Facades\Alert;


class MapelController extends Controller
{

    public function index()
    {
        $mapel = Mapel::all();
        // dd($mapel);
        return view('mapel.index', compact('mapel'));
    }

    public function create()
    {
        return view('mapel.create');
    }

    public function store(Request $request)
    {

        $mapel = new Mapel;

        $mapel->kode_mapel = $request->kode;
        $mapel->nama_mapel = $request->nama;

        $mapel->save();

        Alert::success('Success', 'Berhasil ditambah');
        return redirect('/mapel');
    }

    public function edit($mapel_id)
    {
        $mapel = Mapel::where('id', $mapel_id)->first();
        // dd($mapel);
        return view('mapel.edit', compact('mapel'));
    }

    public function update(Request $request, $mapel_id)
    {
        // dd($mapel_id);
        $mapel = Mapel::find($mapel_id);
        $mapel->nama_mapel = $request['nama'];
        $mapel->save();

        Alert::info('Info', 'berhasil');
        return redirect('/mapel');
    }

    public function destroy($mapel_id)
    {
        $mapel = Mapel::find($mapel_id);
        $mapel->delete();
        Alert::info('Info Title', 'Berhasil');
        return redirect('/mapel');
    }
}
