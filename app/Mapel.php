<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Mapel extends Model
{
    protected $table = 'matapelajaran';

    protected $fillable = ['kode_mapel', 'nama_mapel'];

    public function guru()
    {
        return $this->hasMany('App\Guru');
    }

    public function nilai()
    {
        return $this->hasMany('App\Nilai');
    }
}
