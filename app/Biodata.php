<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $table = 'biodata';
    protected $fillable = [
        'nama',
        'dit_kelas',
        'pada_tgl',
        'semester',
        'jn_kelamin',
        'nik',
        'no_kk',
        'tm_lahir',
        'tgl_lahir',
        'nisn',
        'nis',
        'no_ijazah',
        'th_ijazah',
        'no_skhun',
        'th_skhun',
        'gol_darah',
        'agama',
        'kewargaan',
        'alamat_s',
        'st_dl_kel',
        'no_hp',
        'asal_smp',
        'asal_pindahan',
        'jrk_tinggal',
        't_bdn',
        'b_bdn',
        'email',
        'jml_sdr',
        'jml_sdr_angkat',
        'nm_ayah',
        'ttl_ayah',
        'pen_ayah',
        'pek_ayah',
        'gaji_ayah',
        'alamat_ortu',
        'hdp_mnggl',
        'hp_ortu',
        'nm_ibu',
        'ttl_ibu',
        'pen_ibu',
        'pek_ibu',
        'gaji_ibu',
        'nm_wali',
        'alamat_w',
        'hp_wali',
        'foto_ijazah_sd',
        'foto_ijazah_smp',
        'foto_kk',
        'foto_skl',
        'foto_siswa'
    ];


    public function biodata()
    {
        return $this->hasOne('App\Transkrip');
    }
}
