<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiodataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodata', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('dit_kelas');
            $table->date('pada_tgl');
            $table->string('semester');
            $table->string('jn_kelamin');
            $table->string('nik');
            $table->string('no_kk');
            $table->string('tm_lahir');
            $table->string('tgl_lahir');
            $table->string('nisn');
            $table->string('nis')->nullable();
            $table->string('no_ijazah')->nullable();
            $table->string('th_ijazah')->nullable();
            $table->string('no_skhun')->nullable();
            $table->string('th_skhun')->nullable();
            $table->string('gol_darah')->nullable();
            $table->string('agama');
            $table->string('kewargaan');
            $table->text('alamat_s');
            $table->string('st_dl_kel');
            $table->string('no_hp');
            $table->string('asal_smp');
            $table->string('asal_pindahan')->nullable();
            $table->string('jrk_tinggal');
            $table->string('t_bdn');
            $table->string('b_bdn');
            $table->string('email');
            $table->string('jml_sdr');
            $table->string('jml_sdr_angkat')->nullable();
            $table->string('nm_ayah');
            $table->date('ttl_ayah');
            $table->string('pen_ayah');
            $table->string('pek_ayah');
            $table->string('gaji_ayah');
            $table->string('alamat_ortu');
            $table->string('hdp_mnggl');
            $table->string('hp_ortu');
            $table->string('nm_ibu');
            $table->date('ttl_ibu');
            $table->string('pen_ibu');
            $table->string('pek_ibu');
            $table->string('gaji_ibu');
            $table->string('nm_wali')->nullable();
            $table->text('alamat_w')->nullable();
            $table->string('hp_wali')->nullable();
            $table->string('foto_ijazah_sd');
            $table->string('foto_ijazah_smp')->nullable();
            $table->string('foto_kk');
            $table->string('foto_skl')->nullable();
            $table->string('foto_siswa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodata');
    }
}
