<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranskripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transkrip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor_transkrip', 43)->nullable();
            $table->date('tanggal_transkrip')->nullable();
            $table->unsignedBigInteger('id_biodata');
            $table->foreign('id_biodata')->references('id')->on('biodata');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transkrip');
    }
}
